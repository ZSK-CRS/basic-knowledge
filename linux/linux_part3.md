### 一、meld 使用示例


1. 比较单个文件

```C
meld test.c tmp.c
```

2. 比较目录(或三个)

```C
meld test/ tmp_test/
```

3. 比较版本控制的项目

- meld直接打开文件，会默认显示当前修改与上次的差异
- git difftool 工具默认查找安装的比较工具，比如meld
- git difftool commit1 commit2
- git difftool --dir-diff commit1 commit2
