#!/bin/bash

OriginalFile="src.log"
suffix=".splite.lg"
sizeArray=(4 8 9 10 11 12 13 14 15 16 17 18 19 20 32 64 128 256)
#########################################################################


for size in ${sizeArray[@]}
do
    head -c ${size}k ${OriginalFile} > .${size}${suffix}
done