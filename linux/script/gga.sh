#!/bin/bash

function checkTiming() {
    input=$1;
    echo -e "\033[42;37mGGA时序检测: \033[0m\n"
    cat "$input" | grep --binary-files=text GGA | cut -d ',' -f 2 | awk -F '.' '
	BEGIN {
		result = 0
	} 
	
	{
		h=substr($1,1,2);
		m=substr($1,3,2);
		s=substr($1,5,6);
		curTime=h*3600+m*60+s;
		lose=curTime-preTime;
		preTime=curTime;
		if(lose!=1&&NR!=1&&lose!=-86399)
		{
			result++;
			printf "行号: %s,  UTC : %s   lose : %s\n", NR,$0,(lose-1)
		}
	}

    END {
        if(result==0){
            printf "GGA连续"
        } else {
            printf "\nGGA不连续,共有 %s 处丢包\n",result
        }
    }'
}

function checkDiffAge() {
    input=$1;
    echo -e "\n\033[42;37m差分龄期检测: \033[0m\n"
    cat "$input" | grep --binary-files=text GGA | cut -d ',' -f14 | sort | uniq -c | awk '
    { 
        tag = $2  
        count = $1  
          
        # 如果tag是空字符串,则替换为"nul"  
        if (tag == "") {  
            tag = "nul"  
        }  
          
        # 将tag和count存储在数组中  
        tags[++n] = tag  
        values[n] = count  
    }  
      
    END {  
        printf "%-10s :    %-10s    %s\n\n", "Diffage", "Number", "Proportion"
        # 冒泡排序（降序）  
        for (i = 1; i < n; i++) {  
            for (j = 1; j <= n-i; j++) {  
                if (values[j] < values[j+1]) {  
                    # 交换values和tags中的元素  
                    temp_val = values[j]  
                    values[j] = values[j+1]  
                    values[j+1] = temp_val  
                      
                    temp_tag = tags[j]  
                    tags[j] = tags[j+1]  
                    tags[j+1] = temp_tag  
                }  
            }  
        }  

        for (i = 1; i <= n; i++ ){
            total += values[i]
        }
          
        # 打印排序后的结果  
        for (i = 1; i <= n; i++) {   
            printf "%-10s :    %-10s    %.3f%%\n", tags[i], values[i], (values[i] / total) * 100
            totalDiffage += tags[i]*values[i]
        }  


        printf "\nDiffage mean : %.3f\n",totalDiffage / total
    }'
    
}

while getopts "s:f:" opt
do 
    case $opt in
        s)
            type=$OPTARG
            ;;
        f)
            input=$OPTARG
            ;;
        *)
            exit 1 ;;
    esac
done

# -s : timing 检测GGA时序；diffage 检测GGA差分龄期
# -f : GGA文件路径

if [ "$input" == "" ];then
    echo "input file is null!"
    exit 1
fi

if [ "$type"  == "timing" ];then
    checkTiming $input
elif [ "$type"  == "diffage" ];then
    checkDiffAge $input
else 
    checkTiming $input
    checkDiffAge $input
fi

