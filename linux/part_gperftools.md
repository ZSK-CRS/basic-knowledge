### 一、检查内存

1. 链接 -ltcmalloc 库

2. 代码中添加如下头文件及代码

```C
#include <gperftools/heap-profiler.h>

//开始位置启动分析，参数为生成文件的命名
HeapProfilerStart("out");

//结束位置调用
HeapProfilerStop();

```

3. 编译执行指令

```C
// 表示每分配指定内存后生成一个文件
HEAP_PROFILE_ALLOCATION_INTERVAL=1024*10 ./test.out

// 也可以使用如下指令覆盖代码中分析文件的命名
HEAPPROFILE=custom HEAP_PROFILE_ALLOCATION_INTERVAL=1024*10 ./test.out
```

4. 结果如下

```
out.0001.heap
out.0002.heap
out.0003.heap
out.0004.heap
out.0005.heap
```

5. 结果转换

```C
// pdf
pprof --pdf ./test.out out.0001.heap > out1.pdf
// txt
pprof --text ./test.out out.0001.heap > out1.txt
```

### 二、检测CPU

1. 链接 -lprofiler 库

2. 代码中添加如下头文件及代码

```C
#include <gperftools/profiler.h>

//开始位置启动分析，参数为生成文件的命名
ProfilerStart("cpu");
//结束位置调用
ProfilerStop();

```

3. 编译执行指令

```
//CPUPROFILE_FREQUENCY 采样频率 默认100
CPUPROFILE=./cpu.prof CPUPROFILE_FREQUENCY=500 ./test.out
```