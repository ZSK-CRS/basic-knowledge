### 一、 top指令

``````
1. top              //定位进程
2. top -Hp PID      //定位线程
3. jstack <PID> > thread_dump.txt     //java 方式 (对比查找dump文件中的NID和PID)

``````

### 二、 perf指令

[示例](https://gitee.com/ZSK-CRS/basic-knowledge/blob/master/linux/linux_part2.md)

### 三、Strace指令

[示例](https://gitee.com/ZSK-CRS/basic-knowledge/blob/master/linux/linux_part1.md)