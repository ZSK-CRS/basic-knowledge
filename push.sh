#!/bin/bash
echo "delete build files:"
find . -type f -name "*.o" -print -exec rm {} \;
find . -type f -name "*.out" -print -exec rm {} \;
git add .
git commit -m "$1"
git push