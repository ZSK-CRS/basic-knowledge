#include <stdio.h>

/**
 * 类型转换中的自动转换是指“比较窄的” 操作数转换为 “比较宽的” 操作数，并且不丢失信息
 */


/**
 * @brief 将字符串转换为整型
 * 
 * @param s 
 * @return int 
 */
int lib_atoi(char* s)
{
    int i = 0,n = 0;
    
    for(;s[i] >= '0' && s[i] <= '9';++i){
        n = 10 * n + (s[i] - '0');
    }

    return n;
}

/**
 * @brief 将字符 c 转换为小写形式，只对 ASC字符集有效
 * 
 * @param c 
 * @return int 
 */
int lib_lower(int c)
{
    if(c >= 'A' && c <= 'Z')
        return c + 'a' -'A';
    else
        return c;
}