#include <stdio.h>
#include "type_conversion.h"

int main(int argc,char* argv[])
{
    char * s = "123";
    int n = lib_atoi(s);
    printf("\n lib_atoi = [%d] \n",n);

    char c = 'B';
    int c0 = lib_lower(c);
    printf("\n lib_lower = [%c] \n",c0);

    return 0;
    
}