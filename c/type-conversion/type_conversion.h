#ifndef __TYPE_CONVERSION_H__
#define __TYPE_CONVERSION_H__

#ifdef __cplusplus
extern "C" {
#endif


int lib_atoi(char* s);
int lib_lower(int c);


#ifdef __cplusplus
}
#endif

#endif

