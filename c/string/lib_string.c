
#include <stdlib.h>
#include "lib_string.h"

int lib_str_splite(char* p1, char c, char buffer[20][20], int *count)
{
    char* p = NULL, *pTmp = NULL;
    int tempCount = 0;
    p = p1;
    pTmp = p1;
    
    while (*p!='\0') {
        p = strchr(p, c);
        if (p!=NULL)
        {
            if (p-pTmp>0)
            {
                strncpy(buffer[tempCount],pTmp,p-pTmp);
                buffer[tempCount][p - pTmp] = '\0';
                tempCount++;
                pTmp = p = p + 1;
            }else{
                buffer[tempCount][0] = '\0';
                tempCount++;
                pTmp = p = p + 1;
            }
        }
        else {
            break;
        }
    }
    *count = tempCount;
    return 0;
}