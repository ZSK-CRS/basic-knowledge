#include <stdio.h>
#include "lib_string.h"

int main(int argc,char* argv[])
{
    char * gga = "$GPGGA,023352.55,3113.2180600,N,12148.8000000,E,1,00,1.0,-6.078,M,11.078,M,0.0,*60\r\n";
	char ggaArray[20][20] = {0};
    int num = 0;

    lib_str_splite(gga,',',ggaArray,&num);
	
    char* quality = ggaArray[1];
    
    printf("ggaArray %s\n",quality);
    
    return 0;
    
}