### 一、\_\_attribute__ 使用方式

在定义一个函数、变量或类型时，直接在它们名字旁边添加如下的属性声明即可：

```C
__attribute__((ATTRIBUTE))
```

这里需要注意的是 \__attribute__ 后面是两个小括号，括号里面的 **ATTRIBUTE** 表示要声明的属性，目前支持的属性大概有十几种，接下来介绍主要的几种。


### 二、section

本属性的主要作用是，在程序编译时，将一个函数或变量放到指定的段，即放到指定的 **section** 中。

#### 2.1、section说明

一个可执行文件主要由如下三种段构成。当然还包括只读数据段、符号表等，可以使用 **readelf** 命令查看可执行文件中的各个 seciton 信息

| section    | 组成                    |
|------------|-----------------------|
| 代码段(.text) | 函数定义、程序语句             |
| 数据段(.data) | 初始化的全局变量、初始化的静态局部变量   |
| BSS段(.bss) | 未初始化的全局变量、未初始化的静态局部变量 |

例如下面的程序，我们分别定义一个函数、一个全局变量和一个未初始化的全局变量。

```C
#include <stdio.h>

int global_Val = 8;
int uninit_val;

void print_star(void)
{
    printf("*****\n");
}

int main(int argc,char* argv[])
{
    print_star();    
    return 0;
}

```

使用 **GCC** 编译程序，并查看生成的 a.out 可执行程序的符号表信息

```
gcc -o a.out test.c
readelf -s a.out

符号表信息如下：

   Num:    Value          Size Type    Bind   Vis      Ndx Name

    36: 0000000000000000     0 FILE    LOCAL  DEFAULT  ABS test.c
    ......
    47: 0000000000004018     4 OBJECT  GLOBAL DEFAULT   26 uninit_val
    ......
    53: 0000000000004010     4 OBJECT  GLOBAL DEFAULT   25 global_Val
    ......
    62: 0000000000001149    23 FUNC    GLOBAL DEFAULT   16 print_star

对应的 section header 表信息如下：

  [Nr] Name              Type             Address           Offset
       Size              EntSize          Flags  Link  Info  Align
  
  [16] .text             PROGBITS         0000000000001060  00001060
       0000000000000195  0000000000000000  AX       0     0     16
  [25] .data             PROGBITS         0000000000004000  00003000
       0000000000000014  0000000000000000  WA       0     0     8
  [26] .bss              NOBITS           0000000000004014  00003014
       000000000000000c  0000000000000000  WA       0     0     4
  
```

可以看出，函数 print_star 被放在可执行文件中的 **.text section**, 即代码段；初始化的全局变量 global_val 被放在了 a.out 的 **.data section** , 即数据段；而未初始化的全局变量 uninit_val 则被凡在了 **.bss section** 段中。

现在做如下修改:

```C

int global_Val = 8;
int uninit_val __attribute__((section(".data")));

void print_star(void)
{
    printf("*****\n");
}

int main(int argc,char* argv[])
{
    print_star();    
    return 0;
}

```
此时发现 uninit_val 这个未初始化的全局变量和初始化的全局变量一样，被编译器放在了数据段 **.data** 中


### 三、aligned

**GNU C** 通过 \__attribute__ 来声明 aligned 和 packed 属性，指定一个变量或类型的对齐方式。这两个属性用来告诉编译器：在给变量分配存储空间时，要按照指定的地址对齐方式给变量分配地址。

#### 3.1 什么是地址对齐？

地址对齐是 **CPU** 设计的一种 **时空权衡**，即空间换时间。这里注意地址对齐会导致一定的内存空洞，造成内存资源的浪费，但是这种对齐设计可以简化CPU与内存RAM之间的接口和硬件设计。例如32位的计算机系统，CPU在读取内存时，硬件设计上可能只支持4字节或4字节倍数对齐的地址访问，因此CPU每次想内存RAM读写数据时，一个周期可以读写4字节，如果我们将 **int** 型数据放在4字节对齐的地址上，那么CPU一次就可以将数据读取完毕，如果放在一个非4字节对齐的地址上，那么CPU可能要分两次才能将这个4字节大小的数据读写完毕。


#### 3.2 基本数据类型的地址对齐

一般，编译器会按照默认的地址对齐方式，来为变量分配一个存储空间地址。如下所示

```C
int a1 = 1;
int a2 = 2;
char b1 = 3;
char b2 = 4;

int main(void)
{
    printf("a1: %p\n",&a1);
    printf("a2: %p\n",&a2);
    printf("b1: %p\n",&b1);
    printf("b2: %p\n",&b2);
}
```

结果显示如下：

```C
a1: 0x560ac3faf010
a2: 0x560ac3faf014
b1: 0x560ac3faf018
b2: 0x560ac3faf019
```

由此可知，对于 int 类型数据，编译器按照4字节或4字节的整倍地址对齐；char 类型是 1 字节地址对齐；同理 short 类型回事 2 字节或者2字节整倍地址对齐；

#### 3.3 复合类型数据的地址对齐

编译器在给结构体变量分配存储空间时，不仅要考虑各个成员变量的地址对齐，还要考虑结构体的对齐。同时为了结构体内各个成员地址对齐，编译器可能会在结构体内填充一些空间；为了结构体整体对齐，编译器可能会在结构体的末尾填充一些空间；

```C
struct data
{
    char a;
    int b;
    short c;
};

int main(void)
{
    struct data s;
    printf("size : [%d]\n",sizeof(s));
    printf("a : %p\n",&s.a);
    printf("b : %p\n",&s.b);
    printf("c : %p\n",&s.c);
    return 0;
}
```
运行结果如下所示：

```C
size : [12]
a : 0x7ffcc9a3635c
b : 0x7ffcc9a36360
c : 0x7ffcc9a36364
```

#### 3.4 指定成员地址对齐方式

```C
struct data
{
    char a;
    short b __attribute__((aligned(4)));
    int c;
};

int main(void)
{
    struct data s;
    printf("size : [%d]\n",sizeof(s));
    printf("a : %p\n",&s.a);
    printf("b : %p\n",&s.b);
    printf("c : %p\n",&s.c);
    return 0;
}
```
运行结果如下所示：

```C
size : [12]
a : 0x7ffe41e53b7c
b : 0x7ffe41e53b80
c : 0x7ffe41e53b84
```

显然当上述程序中当不添加 **__attribute__((aligned(4)))** 属性时，由于 char 类型为1字节对齐，short 类型为2字节对齐，而又因为后面的 int 类型为4字节对齐，所以需要在 short 类型后面补充了1字节空间，因此整个结构提大小为8字节；当使用 **__attribute__((aligned(4)))** 显示指定 short 类型为4字节对齐时，就和3.3示例一样，结构体大小变为12字节。


#### 3.5 指定结构体地址对齐方式

除了显示指定结构体中某个成员变量的地址对齐，同时也可以显示整个结构体的对齐方式。

```C
struct data
{
    char a;
    short b;
    int c;
}__attribute__((aligned(16)));

int main(void)
{
    struct data s;
    printf("size : [%d]\n",sizeof(s));
    printf("a : %p\n",&s.a);
    printf("b : %p\n",&s.b);
    printf("c : %p\n",&s.c);
    return 0;
}
```
运行结果如下所示：

```C
size : [16]
a : 0x7ffc7b49d390
b : 0x7ffc7b49d392
c : 0x7ffc7b49d394
```

由前面可知上述结构体中各个成员变量共占8个字节，其中按最大的字节对齐，即4字节对齐；通过 **__attribute__((aligned(16)))** 显示指定结构体整体以16字节对齐，所以编译器就会在这个结构体的末尾填充8字节以满足16字节对齐的要求。

#### 3.6 aligned显示指定对齐一定会生效吗？

这里需要说明一下，这个属性声明其实只是建议编译器按照这种大小地址对齐，但是不能超过编译器允许的最大值。每个编译器对于基本的数据类型都有默认的最大边界对齐字节数，如果超过了，编译器只能按照它规定的最大对齐字节数来给变量分配地址。

```C
char c1 = 3;
char c2 __attribute__((aligned(16))) = 4;
char c3 __attribute__((aligned(32))) = 5;

int main(void)
{
    printf("c1 : %p\n",&c1);
    printf("c2 : %p\n",&c2);
    printf("c3 : %p\n",&c3);
    return 0;
}
```
运行结果如下所示：

```C
c1 : 0x557bfb840020
c2 : 0x557bfb840030
c3 : 0x557bfb840040
```
可以看到，编译器给 **c2、c3** 分配的地址是按照16字节地址对齐的，并不是如所期望的 **c3** 按照32字节地址对齐，这显然是因为超出了编译器所默认的最大边界；

#### 3.7 #pragma pack与__attribute __((aligned)）的区别

- #pragma pack是Microsoft语法，出于兼容性原因，它已被称为ported to GCC，\_\_attribute__((aligned))是GCC特定语法（MSVC不支持）
- #pragma pack（n）会影响结构的每个成员，具体对齐，以指定的 “n” 和默认对齐字节数中较小者为准，可参见下例。
- #pragma pack（和变体）更简洁，并且在GCC语法中表示两个属性packed和aligned
- #pragma pack应用于放置在它被插入的位置之后的每个结构定义（或者直到另一个#pragma pack覆盖它），而\_\_attribute__被本地定义到类型
- #pragma pack的粒度没有属性细：它不能只应用于结构体中的几个成员。但实际上，这很少需要为同一个结构体的成员使用不同的对齐方式设置
- #pragma pack(n)大致相当于\_\_attribute__((packed,aligned(n)))，原则上，#pragma pack可以使用GCC属性来模拟，但不能反过来，因为属性提供了更精细的控制

```C
#pragma pack(2)

struct data
{
    char a;
    short b;
    int c;
};

int main(void)
{
    struct data s;
    printf("size : [%d]\n",sizeof(s));
    printf("a : %p\n",&s.a);
    printf("b : %p\n",&s.b);
    printf("c : %p\n",&s.c);
    return 0;
}
```
运行结果如下所示：

```C
size : [8]
a : 0x7fff5544bf30
b : 0x7fff5544bf32
c : 0x7fff5544bf34
```

上述程序中，char为1字节对齐，小于pack指定的 2，所以是按照 1 字节对齐；无疑short是按照2字节对齐；int为4字节对齐，是大于pack指定的2字节，所以int是按照 2字节对齐；

#### 3.8 对齐的隐患

实际中很多对齐方式是隐式的，例如在强制类型转换的时候：

```C
int main(void)
{
    unsigned int i = 0x12345678;
        
    unsigned char *p = (unsigned char *)&i;
    unsigned short *q = (unsigned short *)(p+1);

    printf("p : %p\n",p);
    printf("q : %p\n",q);
    return 0;
}
```

最后两句代码，是从奇数边界去访问unsigned short型变量，显然不符合对齐的规定。在X86上，类似的操作只会影响效率；但在MIPS或者SPARC(微处理器)上可能导致error，因为它们要求必须字节对齐。

#### 3.9 总结：字节对齐四条规则

1、数据有效对齐值 = min(数据类型的自身对齐值，指定对齐值value)

2、结构体，联合体和类有效对齐值 = min(结构体、联合体和类自身对齐值，指定对齐值value)

3、保证每一个数据满足：不同类型数据都满足：当前数据存储的首地址 % 数据有效对齐值 = 0

4、补齐末尾的空间，保证长度对齐，即总长度 % 实际对齐长度 = 0

注意： 指定对齐值是指系统默认的或者通过 **#pragma pack(value)** 指定对齐value

### 四、packed

aligned 属性一般用来增大变量的地址对齐，元素之间会因此造成一定的内存空洞，而 packed 属性则刚好相反，一般用来减少地址对齐，指定变量或者类型使用最可能小的地址对齐方式

这里需要说明的是 **packed** 属性分别添加到每个成员变量上与直接对整个结构体添加此属性的效果是一样的，如下所示：

```C
struct data                                      struct data              
{                                                {
    char a;                                         char a;
    short b __attribute__((packed));                short b;
    int c __attribute__((packed));                  int c;
};                                               }__attribute__((packed));
```
运行结果如下所示：

```C
size : [7]
a : 0x7ffe790a5e31
b : 0x7ffe790a5e32
c : 0x7ffe790a5e34
```
通过结果可以看出结构体中各个成员地址的分配都是使用最小1字节的对齐方式，没有任何的内存空间的浪费，所以结构体大小为7字节；

在Linux内核中，经常可以看到 **aligned、packed** 一起使用，这样做的好处是：既避免了结构体内各个成员变量因为地址对齐产生内存空洞，又制定了整个结构体的对齐方式。如下所示：

```C
struct data                                                  
{                                   
    char a;
    short b;
    int c; 
}__attribute__((packed,aligned(8)));


int main(void)
{
    struct data s;
    printf("size : [%d]\n",sizeof(s));
    printf("a : %p\n",&s.a);
    printf("b : %p\n",&s.b);
    printf("c : %p\n",&s.c);
    return 0;
}
```

运行结果如下所示：

```C
size : [8]
a : 0x7ffd6067b5e0
b : 0x7ffd6067b5e1
c : 0x7ffd6067b5e3
```
可以看出结构体由于使用了 **packed** 属性，导致结构体所有成员所占的内存空间为7字节，但同时使用了 **aligned(8)** 来指定结构体按照8字节地址对齐，所以编译器要在结构体的后面填充1字节，这样整个结构体的大小就变为8字节，即按照8字节地址对齐；

### 五、weak

#### 5.1 强弱符号概念

对于程序中，所有的变量名、函数名在编译器中都是一个符号，具体可以划分为强符号和弱符号：

- 强符号：函数名，初始化的全局变量名
- 弱符号：未初始化的全局变量名

同一个工程项目中，对于相同的全局变量名、函数名，一般可以归结为如下三种场景：

- 强符号 + 弱符号
- 强符号 + 强符号
- 弱符号 + 弱符号

强符号和弱符号的存在主要用于解决在程序链接过程中，出现多个同名全局变量、同名函数的冲突，这里一般遵循下面3个规则：

- 一山不容二虎
- 强弱可以共处
- 体积大者胜出


#### 5.2 使用方式

5.2.1 声明变量为弱符号

```C
func.c
int a __attribute__((weak)) = 1;

main.c
int a = 4;
...
```
这样在多文件工程中，虽然定义了两个同名变量，但因为在 func.c 中的是弱符号，所以可以共存；假若在两个文件中都是同名弱符号，例如 “int a \_\_attribute__((weak)) = 1”、“short a \_\_attribute__((weak)) = 2”,显然 int 类型占用的体积更大，所以最终选择的是前者

5.2.2 声明函数为弱符号

```C
func.c

void func(void){...}

main.c

void __attribute__((weak)) func(void){...}
```

5.2.3 弱符号的用途

在源文件中引用一个变量或者函数，当编译器只看到声明而没有找到其定义时，编译器一般不会报错，编译器会认为这个符号可能会在其他文件中定义。在链接阶段，链接器会到其他文件中找到这些符号的定义，若是未找到，则报未定义错误。

此时弱符号的作用就体现出来了，当函数呗声明为弱符号时，链接器即使找不到这个函数的定义也不会报错，编译器会将这个函数名设置为0或者一个特殊的值。只有当程序运行调用这个函数的时候，跳转到零地址或者一个特殊的地址才会报错，产生一个内存错误。

这个特性在库函数中利用特别广泛！比如在开发一个库时，基础功能已经实现，某些高级功能还没有实现，那么可以将这些函数通过weak属性声明转换为一个弱符号。通过这些设置，即使还没有实现定义函数，只要我们在调用之前做一个非零判断即可，后续版本发布实现了这些高级功能，应用程序不需要任何修改直接运行即可！


```C

void __attribute__((weak)) func(void);

int main(void)
{
    if(func){
        func();
    }
    return 0;
}
```

### 六、alias

此属性用来给函数定义一个别名，在Linux内核中，alias 有时会和 weak 属性一起使用。必有有些函数随着内核版本升级，函数接口发生了变化，可以通过 alias 属性来对旧的接口名字进行封装，重新起一个接口名字。

```C
// f.c
void __f(void)
{
    printf("__f()\n");
}

void f() __attribute__((weak,alias("__f")));


// main.c
void __attribute__((weak)) f(void);
void f(void)
{
    printf("f()\n");
}

int main(void)
{
    f();
    return 0;
}
```
上面一段代码中，在main.c中新定义了f()函数，那么当main()函数调用 f() 函数时，会直接电泳 main.c 中新定义的函数；当 f() 函数没有被定义时，则会调用 __f() 函数。


### 七、内联函数

所谓内联函数就是为了减少函数调用开销，而将函数声明为内联函数，这样编译器在编译过程中遇到内联函数时，像宏一样将内联函数直接在调用处展开，就不用在保存函数调用现场和回复现场

#### 7.1 内联函数声明

```C
static inline __attritube__((noinline)) int func();
static inline __attritube__((always_inline)) int func();
```

内联函数通常前面会有 static 和 extern 修饰。这里需要说明的是，即使使用了声明，也只是建议编译器去内联展开或者不要内联展开，实际编译器会自己做权衡判断是否展开，这个判断的依据有如下因素：

- 函数体积大小
- 函数体内无指针赋值、递归、循环等语句
- 调用频繁

#### 7.2 内联函数与宏

内联函数与宏作用类似，但为什么不直接定义成宏呢？其原因有如下四条：

- 参数类型检查：内联函数虽然有宏的展开特性，但本质还是函数，所有在编译过程中编译器仍然可以对其进行参数检查，而宏不具备这个功能
- 便与调试：函数支持的调试功能有断点、单步等，内联函数同样支持
- 返回值： 内联函数有返回值，这个是相对于C 89来说的，现在宏也可以有返回值
- 接口封装： 某些内联函数可以用来封装一个接口，而宏不具备这个特性

#### 7.3 内联函数的缺点

内联函数会增大程序的体积，如果在一个文件中多次调用内联函数，就会在造成多次展开，这样整个程序中的体积就会变大，在一定程度上降低程序的执行效率。同时函数的作用是为了提高代码的复用性，内联函数往往又降低了函数的复用性。

#### 7.4 内联函数为什么定义在头文件中？

在linux内核中，会看到大量的内联函数定义在头文件中，并且被static修饰，这是为什么呢？

首先，之所以定义在头文件中，是为了使用方便，任何想使用这个内联函数的源文件直接包含头文件即可。其次使用static修饰，是因为在使用inline定义的内联函数，编译器并不一定会内联展开，那么当一个工程中多个文件都包含这个内联函数的定义时，编译器可能会报重复定义的错误，此时使用static来修饰，则可以将这个函数的作用域限制在各自的文件内，避免这个错误发生

### 八、内建函数

内建函数是编译器内部实现的函数，通常以 **_builtin** 开头。这些函数主要在编译器内部使用，主要为了编译器服务的，有些是为了优化编译，这里以 **"__builtin_expect"** 来做说明。此函数是用来做一些分支上的预测。在linux内核中，定义了如下两个宏：

```C
#define likely(x) __builtin_expect(!!(x),1)
#define unlikely(x) __builtin_expect(!!(x),0)
```

这两个宏的主要作用是告诉编译器，某一个分支的发生的概率很高或者很低。编译器根据这个提示信息，在编译程序时就会做一些分支预测上的优化，例如**if/switch**这种结构，会动态的将大概率发生的事情挪移到前面。

```C
 
int main(void)                  int main(void)
{                               {
    int a;                          int a;
    scanf("%d",&a);                 scanf("%d",&a);
    if(a == 0)                      if(unlikely(a == 0))
    {                               {
        printf("%d",1);                 printf("%d",1);
        printf("%d",2);                 printf("%d",2);
        printf("\n");                   printf("\n");
    }else {                         }else {
        printf("%d",5);                 printf("%d",5);
        printf("%d",6);                 printf("%d",6);
        printf("\n");                   printf("\n");
    }                               }
    return 0;                       return 0;
}                               }
```
通过反汇编可以看出当使用unlikely修饰if分支，告诉编译器这个分支是小概率发生，则在编译器开启编译优化的条件下，编译器会将if分支汇编代码放到下面，而将大概率发生的else分支的汇编放到上面，这就确保了程序在执行时，大部分时间不需要跳转，提高缓存的命中率。



