通过 **typeof** 获取一个变量的类型之后，可以使用该类型再定义一个变量，这和直接使用该类型定义变量是一样的，这里直接用示例来做说明：

```C
int main(int argc,char* argv[])
{
    int i = 2;
    typeof(i) k = 6;

    int *p = &k;
    typeof(p) q = &i;

    printf("k = %d\n",k);
    printf("*p = %d\n",*p);
    printf("i = %d\n",i);
    printf("*q = %d\n",*q);

    return 0;
}

```

上述基本数据类型的结果如下：

```C
k = 6
*p = 6
i = 2
*q = 2
```

当然除了基本数据类型之外，还有一些其他高级的用法，如下所示：

```C
typeof (int *) y;     //把y定义指向int类型的指针，相当于int *y
typeof (int) *y;     //定义一个指向int类型的指针变量
typeof (*x) y;      //定义一个指针x所指向类型的变量y
typeof (int) y[4];     //定义一个 int y[4]
typeof (*x) y[4];     //把y定义位指针x指向的数据类型的数组
typeof (typeof(char*)[4]) y;     //相当于定义字符指针数组 char *y[4]
typeof (int x[4]) y;     //相当于定义 int y[4]

```