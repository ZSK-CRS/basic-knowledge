##  <center> Vim 操作命令大全

### 一、vim的几种模式

- 正常触：可以使用快捷键命令，或按:输入命令行.
- 插入模式：可以输入文本，在正常模式下，按i、a、o等都可以进入插入模式.
- 可视模式：正常触下按v可以进入可视模式，在可视模式下，移动光标可以选择文本。按V进入可视行模式，总是整行整行的选中。ctrl+v入可视块模式
- 替换模式：正常模式下，按R进入s

想要撤回编辑模式下的操作，需要先退出编辑模式，再按u键

### 二、启动vim

- vim -c cmd file:在打开文件前，阳丸行指定的命令；
- vim -r file:恢复上次异常退出的文件；
- vim -R file:以只读的方式打开文件，但可以园制保存；
- vim -M file:以只读的方式打开文件，不可以强制保存；
- vim -y num file:将编辑窗口的大小设为num行；
- vim + file:从文件的末尾开始；
- vim + num file:从第num行开始；
- vim + /string file:打开file,并将光标停留在第一个找到的string上。
- vim -remote file:用已有的vim进程打开指定的文件.如果你不想启用多个vim会话，这个很有用.但要注意，如果你用vim,会寻找名叫VIM的服务器；如果你已经有一gvim在运行了，你可以用gvim -remote file在已有的gvim中打开文件

### 三、文档操作

- :e file 关闭当前编辑的文件，并开启新的文件.如果对当前文件的修改未保存，vi会警告.
- :e!file 放弃对当前文件的修改，编辑新的文件.
- :e+file 开始新的文件，并从文件尾开编辑
- :e+n file 开始新的文件，并从第n行开始编辑.
- :enew 编译一未命名的新文档.（ CTRL-W n）
- :e ”重新加载当前文档.
- :e! 重新加载当前文档，并丢弃已做的改动.
- :e#或ctrl+^ 回到刚才编辑的文件，很实用
- :f或ctrl+g 显示文档名，是否修改，和光标位置.
- :f filename 改变编辑的文件名，这时再保存相当于另存为
- :x 保存并退出
- :q 退出当前窗口.（CTRL-W 减CTRL-W CTRL-Q）
- :saveas newfilename 另存为
- :browse e 会打开T文件浏览系统选择要编辑的文件，如果是终端中，则会打开netrw的文件浏览窗口；如果是gvim，则会打开一个图形界面的浏览窗口。实际上:browse后可以根任何编辑文档的命令，如sp等。用browse打开的起始目录可以由browsedir来设置
辑文档的命令，如sp等,用browse打开的起始目录可以由browsedir来设置：
  - :set browsedir=last-用上次访问过的目录（默认）；
  - :set browsedir=buffer-用当前文件所在目录；
  - :set browsedir=current-用当前工作目录；
- :E(Explore) 将在当前窗口中打开文件浏览器
- :Sex(Sexplore) 水平分割T窗口，浏览文件夹，可以直接在此指令后面添加目录打开；
- :Vex(Vexplore) 垂直分割T窗口，浏览文件系统；
- :edit E:\_ToDo 通过编辑目录来浏览该文件夹

### 四、多窗口多文件编辑

- vim -O file1 file2 ... 垂直分屏
- vim -o file1 file2 ... 水平分屏
- :sp 将当前文件水平分屏
- :sp file 创建新文件并水平分屏
- :vsp 将当前文件垂直分屏
- :vsp file 创建新文件并垂直分屏
- Ctrl+w+方向键 或者 Ctrl+w+h/j/k/l 朝某个方向切换窗格
- Ctrl+w w 按创建顺序切换到下一个窗格中
- :qall 关闭所有窗口
- :wall 保存所有修改过的窗口
- :only 只保留当前窗口，关闭其他窗口
- vim file1 file2 或 vim /../.. 
- :files 列出当前打开的文件列表
- :n 编辑下一个文件
- :N 编辑上一个文件

### 五、多标签编辑

- vim -p files: 打开多个文件，每个文件占用一个标签页。
- :tabe tabnew – 如果加文件名，就在新的标签中打开这个文件， 否则打开一个空缓冲区
- :q 关闭当前标签页
- :qa 关闭所有标签页
- :tab split – 将当前缓冲区的内容在新页签中打开。
- :tabc[lose] – 关闭当前的标签页。
- :tabo[nly] – 关闭其它的标签页。
- :tabs – 列出所有的标签页和它们包含的窗口。
- :tabm[ove] [N] – 移动标签页，移动到第N个标签页之后。 如 tabm 0 当前标签页，就会变成第一个标签页。

### 六、光标移动

以下移动都是在normal模式下

- h或退格:左移一个字符；
- I或者空格:右移一个字符；
- j：下移一行；
- k：上移一行；
- +或Enter:把光桁移至下一行第一M院白字符.
- w：前移一个单词，光标停在下一个单词开头；
- W：移动下一个单词开头，但忽略一些标点；
- nG:到文件第n行.
- :n移动到第n行.
- :$移动到最后一行.
- H:把光标移至屏幕最顶端一行.
- M:把光标移到屏幕中间一行.
- L:把光标移到屏幕最僦T亍.
- gg：至技件头部.
- G:到文件尾部.

### 七、标记

- m ——创建标记
- ' ——移动到标记的文本行首
- ` ——移动到标记的光标位置
- :marks ——列示所有标记
- :delmarks ——删除指定标记
- :delmarks! ——删除所有标记

将光标移到某一行，使用 ma 命令添加标记。其中，m 是标记命令，a 是所做标记的名称。

可以使用小写字母 a-z 或大写字母 A-Z 中的任意一个做为标记名称。小写字母的标记，仅用于当前缓冲区；而大写字母的标记，则可以跨越不同的缓冲区。例如，你正在编辑 File1，但仍然可以使 用'A 命令，移动到 File2 中创建的标记A。

使用标记可以快速移动。到达标记后，可以用ctrl+o回原来的位置。Ctrl+o和Ctrl+i很像浏览器上的后退和前进.

利用:marks命令，可以列出所有标记。这其中也包括一些系统内置的特殊标记（Special marks）：
- . ——最近编辑的位置
- 0-9 ——最近使用的文件
- ∧ ——最近插入的位置
- ' ——上一次跳转前的位置
- " ——上一次退出文件时的位置
- [ ——上一次修改的开始处
- ] ——上一次修改的结尾处

### 八、插入文本

- a:在光标后播入；
- A:在当前行最后播入；
- o:在下面新建一行播入；
- O:在上面新建一行播入；
- :r filename 在当前位置插入另一个文件的内容
- :[n]r filename 在第n行插入另一个文件的内容.
- :r!date 在光标处播入当前日期与时间.同理，：r ！command可以将其它shell命令的输出插入当前文档

### 九、剪切复制粘贴操作

- [n]x: 剪切光标右边n个字符，相当于d[n]l。
- [n]X: 剪切光标左边n个字符，相当于d[n]h。
- y: 复制在可视模式下选中的文本。
- yy or Y: 复制整行文本。
- y[n]w: 复制一(n)个词。
- y[n]l: 复制光标右边1(n)个字符。
- y[n]h: 复制光标左边1(n)个字符。
- y: 从光标当前位置复制到行尾。
- y0: 从光标当前位置复制到行首。
- :m,ny 复制m行到n行的内容。
- y1G或ygg: 复制光标以上的所有行。
- yG: 复制光标以下的所有行。
- yaw和yas：复制一个词和复制一个句子，即使光标不在词首和句首也没关系。
- d: 删除（剪切）在可视模式下选中的文本。
- d$: 删除（剪切）当前位置到行尾的内容。
- d[n]w: 删除（剪切）1(n)个单词
- d[n]l: 删除（剪切）光标右边1(n)个字符。
- d[n]h: 删除（剪切）光标左边1(n)个字符。
- d0: 删除（剪切）当前位置到行首的内容
- [n] dd: 删除（剪切）1(n)行。
- :m,nd 剪切m行到n行的内容。
- d1G或dgg: 剪切光标以上的所有行。
- dG: 剪切光标以下的所有行。
- daw和das：剪切一个词和剪切一个句子，即使光标不在词首和句首也没关系。
- d/f：这是一个比较高级的组合命令，它将删除当前位置 到下一个f之间的内容。
- p: 在光标之后粘贴。
- P: 在光标之前粘贴。
- y, d, c, v都可以跟文本对象。

### 十、查找与替换操作

- :/something 在后面的文本中查找something.
- :?something 在前面的文本中查找something.
- :/pattern/+number 将光标停在包含 pattern 的行后面第 numbe 行上
- :/pattern/-number 将光标停在包含 pattern 的行前面第 numbe 行上
- n:向后查找下一个
- N:向前查找下一个

- :s/old/new - 用new替换当前行第一个old。
- :s/old/new/g - 用new替换当前行所有的old。
- :n1,n2s/old/new/g - 用new替换文件n1行到n2行所有的old。
- :%s/old/new/g - 用new替换文件中所有的old。
- :%s/^/xxx/g - 在每一行的行首插入xxx，^表示行首。
- :%s//xxx/g−在每一行的行尾插入xxx，/xxx/g−在每一行的行尾插入xxx，表示行尾。
  
所有替换命令末尾加上c，每个替换都将需要用户确认。 如：%s/old/new/gc，加上i则忽略大小写(ignore)。、

### 十一、编程辅助

- gd: 跳转到局部变量的定义处；
- gD: 跳转到全局变量的定义处，从当前文件开头开始搜索；
- g;: 上一个修改过的地方；
- g,: 下一个修改过的地方；
- [[: 跳转到上一个函数块开始，需要有单独一行的{。
- ]]: 跳转到下一个函数块开始，需要有单独一行的{。
- []: 跳转到上一个函数块结束，需要有单独一行的}。
- ][: 跳转到下一个函数块结束，需要有单独一行的}。
- [{: 跳转到当前块开始处；
- ]}: 跳转到当前块结束处；
- [/: 跳转到当前注释块开始处；
- ]/: 跳转到当前注释块结束处；
- %: 不仅能移动到匹配的(),{}或[]上，而且能在#if，#else， #endif之间跳跃。



### 十二、万能操作

如果你找不到合适的Vim命令，随时都可以在Vim中直接执行Bash命令。 只需要在Ex模式中添加前导的!字符，例如：

- :!ls 列出文件
- :!rm foot.c 删除文件


