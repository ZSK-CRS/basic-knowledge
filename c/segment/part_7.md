### 一、container_of 宏定义

```
#define offsetof(TYPE,MEMBER)((size_t)&((TYPE*)0)->MEMBER)
#define container_of(ptr, type, member) ({			\
	const typeof( ((type *)0)->member ) *__mptr = (ptr);	\
	(type *)( (char *)__mptr - offsetof(type,member) );})

注意，这里 (size_t)&((TYPE*)0)->MEMBER 中的 “&” 是取地址符，并不是 ‘与操作‘
```

### 二、实现原理

编译器在给结构体变量在内存中分配存储空间时，是会根据结构体中每个成员变量的数据类型和字节对齐方式来按顺序分配一段连续的空间来存储。如下所示：

```C
struct student
{
    int age;
    int num;
    int math;
};

int main(int argc,char* argv[])
{
    struct student stu = {20,1001,99};
    printf("&stu = %p\n",&stu);
    printf("&stu.age = %p\n",&stu.age);
    printf("&stu.num = %p\n",&stu.num);
    printf("&stu.math = %p\n",&stu.math);
    return 0;
}

```

上述结果如下,可见分配的空间是连续的：

```C
&stu        = 0028FF30
&stu.age    = 0028FF30
&stu.num    = 0028FF34
&stu.math   = 0028FF38

```


结构体数据类型在同一个编译环境下，各个成员变量相对于结构体首地址的偏移是固定不变的，可以试想，当结构体的首地址为 0 时，结构体中各个成员的地址在数值上等于结构体各成员相对于结构体首地址的偏移。

```C
struct student
{
    int age;
    int num;
    int math;
};

int main(int argc,char* argv[])
{
    printf("&age = %p\n",&((struct student*)0)->age);
    printf("&num = %p\n",&((struct student*)0)->num);
    printf("&math = %p\n",&((struct student*)0)->math);
    return 0;
}

```

在上述程序中，没有直接定义结构体变量，而是将数字 **0** 通过强制类型转换，转换为一个指向结构体类型为 **student** 的常量指针，然后分别打印这个常量指针指向的各成员地址，结果如下：

```C
&age   = 00000000
&num   = 00000004
&math  = 00000008

```

如此，我们可以得到这个关键字最本质的原理： **取结构体某个成员变量member的地址，减去这个成员在结构体中的偏移，运算结果即为结构体的首地址**； 在上述 container_of 宏定义中，采用语句表达式的形式定义。

### 三、使用示例

```C
struct student
{
    int age;
    int num;
    int math;
};

int main(int argc,char* argv[])
{
    struct student stu = {20,1001,99};
    struct student *p;
    p = container_of(&stu.num,struct student,num);
    
    return 0;
}

```
