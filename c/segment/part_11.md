### 一、类的C语言模拟实现

面向对象编程是一种思想，和编程语言没有关系。虽然C语言中没有class关键字，但是可以使用结构体来模拟一个类；为了解决结构体中不能直接定义函数的问题，我们采用在结构体中内嵌函数指针来模拟类中的函数。如下所示，定义一个Animal类：

```C
struct func_operations
{
    void (*fp1)(void);
    void (*fp2)(void);
    void (*fp3)(void);
    void (*fp4)(void);
};

struct animal
{
    int age;
    int weight;
    struct func_operations fp;
};
```
通过以上的封装，我们就可以把一个类的属性和方法都封装在一个结构体中。此时这个封装后的结构体就相当于一个“类”，那么子类该如何继承呢？

#### 1.1 通过嵌入结构体继承

```C
struct cat
{
    struct animal *p;
    struct animal ani;
    char sex;
};
```
C语言可以通过结构体中内嵌另一个结构体或结构体指针来模拟类的继承。如上所示，通过在cat里内嵌结构体 animal，此时结构体 cat 就拥有了 animal 的所有能力，这就相当于模拟了一个子类 cat ，animal 结构体就相当于一个父类，可以简单测试如下：

```C
#include <stdio.h>

struct func_operations
{
    void (*fp1)(void);
    void (*fp2)(void);
    void (*fp3)(void);
    void (*fp4)(void);
};

struct animal
{
    int age;
    int weight;
    struct func_operations fp;
};

struct cat
{
    struct animal *p;
    struct animal ani;
    char sex;
};

void speek(void)
{
    printf("animal speeking...\n");
}

int main(void)
{
    struct animal ani;
    ani.age = 1;
    ani.weight = 2;
    ani.fp.fp1 = speek;

    struct cat c;
    c.p = &ani;
    c.p->fp.fp1();
    return 0;
}
```

#### 1.2 通过私有指针继承

在 animal 中定义一个私有指针

```C
struct animal
{ 
    int age;
    int weight;
    struct func_operations fp;
    void * p;
};
```

子类集成父类的属性，可以如下所示：

```C
struct animal
{ 
    int age;
    int weight;
    void * p;
};

typedef struct cat
{
    int score;
};

int main(void)
{
    struct animal ani;
    ani.age = 1;
    ani.weight = 2;

    ani.p = malloc(sizeof(struct cat));
    ((struct cat*)ani.p)->score = 10;
    return 0;
}
```

这样通过定义私有指针 **\*p** 来扩展子类的属性。

#### 1.3 多态

这里简单说明使用C语言实现的多态，所谓多态是指接收同样的指令却发生不同的动作；C语言中，我们举例“插件化”，通过定义统一的行为接口，而不同的协议有不同的实现，这就是多态。这里不再多说

### 二、链表的抽象与封装

链表是一种常用的动态数据结构，一般一个链表节点包含两部分内容：数据域和指针域；在实编程中，根据业务需求，不同的链表节点可能会封装不同的数据域，构成不同的数据格式，进而连成不同的链表

不同的链表虽然数据域不同，但是基本的操作都是相同，因此Linux内核中为了实现对链表操作的代码复用，定义了一个通用的链表及相关操作：

```C
struct list_head
{
    struct list_head *next,*prev;
};

void INIT_LIST_HEAD(struct list_head *list);
int list_empty(const struct list_head *head);
void list_add(struct list_head *new,struct list_head *head);
void list_del(struct list_head *entry);
void list_replace(struct list_head *old,struct list_head * new);
void list_move(struct list_head *list,struct list_head *head);
```

假如将结构体类型 list_head 及相关的操作看成一个基类，如果子类要继承子类的属性和方法，直接将 list_head 内嵌到自己的结构体内即可，如下所示：

```C
struct my_list_node
{
    int data;
    struct list_head list;
}
```
