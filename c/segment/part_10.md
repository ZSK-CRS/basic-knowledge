### 一、 可变参数宏形式

可变参数宏在 C99 中已经支持此特性，但因为其他编译器并不能很好的支持这个特性，所以可以认为是 GNU C的一个语法扩展。

```C
//形式1
#define LOG(fmt,...) printf(fmt,##__VA_ARGS__)
//形式2
#define LOG(fmt,args...) printf(fmt,##args)
```

上面两种形式需要注意的是：

- \_\_VA_ARGS__ 是C99的标准写法，形式2是 GNU C扩展的新写法
- \#\# 是为了避免参数列表为空的语法错误

### 二、内核中的可变参数宏

这里不讨论此宏的作用，只是针对一个知识点来做说明，如下所示：

```C
#define dynamic_pr_debug(fmt, ...)                \
do {                                \
    DEFINE_DYNAMIC_DEBUG_METADATA(descriptor, fmt); \
    if (unlikely(descriptor.flags       \
            & _DPRINTK_FLAGS_PRINT))    \
        __dynamic_pr_debug(&descriptor, pr_fmt(fmt),    \
                   ##__VA_ARGS__);      \
} while (0)
```

为什么要用 **do{...} while (0)** 包含代码块呢？看起来这个并没有什么作用，但其实是为了防止宏在条件、选择等分支结构的语句中展开后产生的宏歧义；

例如我们这样定义一个宏：

```C
#define DEBUG() \
 printf("hello ");printf("else\n")

int main(void)
{
    if(1)
        printf("hello if\n");
    else
        DEBUG();
    return 0;
}
```

运行结果为：

```C
hello if
else
```

理论情况下else分支是执行不到，但在上述代码中也执行了else分支的一部分，这是因为我们定义的宏有多条语句组成，直接展开后，就变成了下面这样：

```C
int main(void)
{
    if(1)
        printf("hello if\n");
    else
        printf("hello ");
        printf("else\n");
    return 0;
}
```
多条语句在宏调用处直接展开，就破坏了程序原来的 if-else 分支结构，导致程序逻辑发生变化，所以你才会看到 else 分支的非正常打印。而采用 do{ … }while(0) 这种结构，可以将我们宏定义中的复合语句包起来，宏展开后，就是一个代码块，就避免了这种逻辑错误