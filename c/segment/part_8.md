### 一、变长数组

C99 标准规定，可以定义一个变长数组，即数组的长度在编译时并不确定，在运行时才指定大小。

```C
int len;
int a[len];
```

### 二、零长度数组示例

```C
struct buffer
{
    int len;
    int a[0];
};


int main(int argc,char* argv[])
{
    struct buffer *buf;
    buf = (struct buffer*)malloc(sizeof(struct buffer)+20);
    buf->len = 20;
    strcpy(buf->a,"hello world\n");
    puts(buf->a);

    free(buf);
    return 0;
}

```

上述程序中，使用 **malloc** 申请一片内存，大小为 sizeof(buffer)+20, 一共24个字节。其中 4 个字节用来保存内存的长度20，剩下的20字节空间，才是真正可以使用的内存空间，这样我们可以通过结构体成员 a 直接访问这片内存。


### 三、为什么使用零长度数组

这里我们以内核中 USB 驱动中的示例来做说明。一般零长度数组并不单独使用，而是以变长结构体的形式出现。在 USB 驱动中有一个类似套接字缓冲区的东西，叫做 **URB**，全名为 USB Request Block，即 USB 请求模块，用来传输 USB 数据包。

```C
struct urb {
    /* private: usb core and host controller only fields in the urb */
    struct kref kref;        /*urb 引用计数  */
    void *hcpriv;            /* 主机控制器私有数据 */
    atomic_t use_count;        /* 当前提交的总数*/
    atomic_t reject;        /* submissions will fail */
    int unlinked;            /* unlink error code */

    /* public: documented fields in the urb that can be used by drivers */
    struct list_head urb_list;    /* urb链表头
                     * current owner */
    struct list_head anchor_list;    /* the URB may be anchored */
    struct usb_anchor *anchor;
    struct usb_device *dev;        /*关联的 USB 设备 */
    struct usb_host_endpoint *ep;    /* (internal) pointer to endpoint */
    unsigned int pipe;        /* 管道 */
    unsigned int stream_id;        /* (in) stream ID */
    int status;            /* urb的状态 */
    unsigned int transfer_flags;    /* (in) URB_SHORT_NOT_OK | ...*/
    void *transfer_buffer;        /*发送数据到设备或从设备接收数据的缓冲区*/
    dma_addr_t transfer_dma;    /* 用来以 DMA 方式向设备传输数据的缓冲区*/
    struct scatterlist *sg;        /* (in) scatter gather buffer list */
    int num_mapped_sgs;        /* (internal) mapped sg entries */
    int num_sgs;            /* (in) number of entries in the sg list */
    u32 transfer_buffer_length;    /* transfer_buffer 或 transfer_dma 指向缓冲区的大小 */
    u32 actual_length;        /*URB 结束后，发送或接收数据的实际长度 */
    unsigned char *setup_packet;    /*指向控制 URB 的设置数据包的指针*/
    dma_addr_t setup_dma;        /*控制 URB 的设置数据包的 DMA 缓冲区*/
    int start_frame;        /* 等时传输中用于设置或返回初始帧 */
    int number_of_packets;        /* 等时传输中等时缓冲区数据 */
    int interval;            /*URB 被轮询到的时间间隔（对中断和等时 urb 有效）*/
    int error_count;        /* 等时传输错误数量  */
    void *context;            /* completion 函数上下文 */
    usb_complete_t complete;    /*当 URB 被完全传输或发生错误时，被调用 */
    struct usb_iso_packet_descriptor iso_frame_desc[0];
                    /*单个 URB 一次可定义多个等时传输时，描述各个等时传输 */
}

```

这个结构体中定义了 USB 数据包的传输方向、传输地址、传输大小、传输模式等，这里主要看最后定义的成员：

```C
struct usb_iso_packet_descriptor iso_frame_desc[0]

```

这个零长度数组主要用于 USB 的同步传输。例如 USB 摄像头一般支持多种分辨率，不同的分辨率的视频传输，一帧图像的数据大小是不一样的，同样对于 USB 传输数据包的大小和个数也是不一样的。零长度数组就是为了适配这种需求而不影响其他因素。USB 驱动可以根据一帧图像数据的大小，灵活的申请内存空间，来满足不同大小的数据传输同时零长度数组不占用结构体的存储空间。


### 四、为什么不使用指针代替零长度数组

- 指针本身占用空间
- 零长数组不会对结构体定义造成冗余，而且使用方便



