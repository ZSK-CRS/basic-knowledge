#include <stdio.h>

//这里我们定义一个宏，来求两个数的最大值


/**
 * @brief 方式一
 * 不适用 MAX(1 != 1，1 ！= 2)
 * 展开后表现为 ：  1!=1>1!=2?1!=1:1!=2
 * > 优先级为 6，大于 ！= (优先级为7)
 */
#define Max(x,y) x > y ? x : y

/**
 * @brief 方式二
 * 不适用 3+MAX(1，2)
 * 展开后表现为 ：  3+(1)>(2)?(1):(2)
 * 结果为 1，不是5
 */
#define Max(x,y) (x) > (y) ? (x) : (y)

/**
 * @brief 方式三
 * 不适用 i=1,j=2, MAX(i++，j++)
 * 展开后表现为 ：  前者和后者做了两次自增运算 
 * 结果为 7，不是6
 */
#define Max(x,y) ((x) > (y) ? (x) : (y))


/**
 * @brief 方式四
 * 不适用 参数为非int类型
 * 语句表达式，定义临时变量来避免上述问题
 */
#define Max(x,y) ({ \
    int _x = x;     \
    int _y = y;     \
    _x > _y ? _x : _y; \
})


/**
 * @brief 方式五： Linux内核中定义 min_t 和 max_t 时使用的就是如下方式
 * 添加type来支持任意类型，例如 MAX(float,1,3), 单考虑可以省略type
 */
#define Max(type,x,y) ({ \
    type _x = x;     \
    type _y = y;     \
    _x > _y ? _x : _y; \
})


/**
 * @brief 方式六 ： 最优解
 * 使用typeof关键字，这是GNU C新增的一个关键字
 * (void)(&_x == & _y) 作用有两个：
 * 1、给用户一个警告，对于不同类型的指针比较，编译器会发出警告，提示两种数据类型不同
 * 2、若是两个数进行比较，运算结果没有用到，某些编译器可能会给一个警告，加一个(void)之后可以消除
 */
#define Max(x,y) ({ \
    typeof(x) _x = x;     \
    typeof(y) _y = y;     \
    (void)(&_x == & _y);  \  
    _x > _y ? _x : _y; \
})
