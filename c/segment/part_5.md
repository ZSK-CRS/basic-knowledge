### 一、指定初始化数组元素


1.1、数组中通过索引来初始化


```C
int a[100] = { [10] = 1, [30] = 2};
```

1.2、数组中给索引范围内的元素初始化

```C
int a[100] = { [10...30] = 1, [50...60] = 2};
```

**GNU C** 支持使用 **...** 表示范围扩展，这个特性不仅仅可以使用在数组中，同样可以使用在 **switch-case** 语句中

1.3、switch-case 初始化

```C
int main(int argc,char* argv[])
{
    int i = 4;

    switch(i){
        case 1:
            printf("1\n");
            break;
        case 2...8:
            printf("%d\n",i);
            break;
        case 9:
            printf("9\n");
            break;
        default:
            printf("default\n");
            break;
    }
    return 0;
}
```

### 二、指定初始化结构体成员

在早期 C 标准中，初始化结构体变量也要按照固定的顺序，但是在 GNU C 中可以直接通过结构体域来指定初始化某个成员。

```C
struct student {
    char name[20];
    int age;
    int (*say)(char* message);
}

int saySomething(char* message)
{
    printf("%s\n",message);
}

int main(int argc,char* argv[])
{
    struct student stu1 = {"张三",30,saySomething};

    struct student stu2 = {
        .name = "李四",
        .age = 23,
        .say = saySomething
    };

    ...
}
```