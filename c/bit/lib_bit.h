#ifndef __LIB_STRING_H__
#define __LIB_STRING_H__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <string.h>
#include <stdint.h>

int32_t getbits(const uint8_t* buff,int pos,int len);
uint32_t getbitu(const uint8_t* buff,int pos,int len);
void setbitu(uint8_t* buff,int pos,int len,uint32_t data);
void setbits(uint8_t* buff,int pos,int len,int32_t data);


#ifdef __cplusplus
}
#endif

#endif

