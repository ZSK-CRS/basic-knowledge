#include <stdio.h>
#include "lib_bit.h"

int main(int argc,char* argv[])
{
    int index = 0;
    char target[128];
    memset(target,0x0,sizeof(target));

    char num[64] = "456";
    for(int i = 0; i < strlen(num);i++)
    {
         setbitu(target,index,8,num[i]);
         index += 8;
    }
    

#if 0

    uint32_t s1 = 0x7E;
    setbitu(target,0,8,s1);

    uint32_t r1 = getbitu(target,0,8);
    printf("r1 = %x\n",r1);


    int32_t s2 = -1;
    setbits(target,8,8,s2);

    int32_t r2 = getbits(target,8,8);
    printf("r2 = [%d]\n",r2);

    char *s3 = "zsk";
    memcpy(target+2,s3,strlen(s3)+1);

    printf("r3 = [%s]\n",target);

 
#endif 
   
    for (int i = 0; i < strlen(target); i++)
    {
       printf("%x",target[i]);
    }
    
    printf("\n");
    
}