#include <stdio.h>
#include "lib_bit.h"

/**
 * @brief set unsigned bits to byte data
 * 
 * @param buff IO byte data
 * @param pos I   bit position from start of data (bits)
 * @param len I   bit length (bits) (len<=32)
 * @param data I   unsigned data
 */
void setbitu(uint8_t* buff,int pos,int len,uint32_t data)
{
    uint32_t mask = 1u << (len - 1);
    int i;
    if (len <= 0 || 32 < len) return;
    for (i = pos; i < pos + len; i++, mask >>= 1) {
        if (data & mask) {
            buff[i / 8] |= 1u << (7 - i % 8);
        } else {
            buff[i / 8] &= ~(1u << (7 - i % 8));
        } 
    }
}

/**
 * @brief set signed bits to byte data
 * 
 * @param buff IO byte data
 * @param pos I   bit position from start of data (bits)
 * @param len I   bit length (bits) (len<=32)
 * @param data I   signed data
 */
void setbits(uint8_t* buff,int pos,int len,int32_t data)
{
    if (data < 0) {
        data |= 1 << (len - 1); 
    } else {
        data &= ~(1 << (len - 1));
    }
    setbitu(buff, pos, len, (uint32_t)data);
}



/**
 * @brief 
 * 
 * @param buff : I byte data
 * @param pos : I bit position from start of data (bits)
 * @param len : I bit length (bits) (len<=32)
 * @return int : O extracted unsigned/signed bits
 */
uint32_t getbitu(const uint8_t* buff,int pos,int len)
{
    uint32_t bits = 0;
    int i;
    for (i = pos; i < pos + len; i++) {
        bits = (bits << 1) + ((buff[i / 8] >> (7 - i % 8)) & 1u);
    }
    return bits;
}


/**
 * @brief 
 * 
 * @param buff : I byte data
 * @param pos : I bit position from start of data (bits)
 * @param len : I bit length (bits) (len<=32)
 * @return int : O extracted unsigned/signed bits
 */
int32_t getbits(const uint8_t* buff,int pos,int len)
{
    uint32_t bits = getbitu(buff, pos, len);
    if (len <= 0 || 32 <= len || !(bits & (1u << (len - 1)))) {
        return (uint32_t)bits;
    }
    return (uint32_t)(bits | (~0u << len)); 
}