package cn.zsk.plugin;

import org.gradle.api.Action;
import org.gradle.api.NamedDomainObjectContainer;
import org.gradle.util.internal.ConfigureUtil;
import groovy.lang.Closure;
import org.gradle.api.Project;

public class MyExtension {
    String pluginName;
    int version;
    DefaultConfig defaultConfig = new DefaultConfig();

    // 添加 BuildTypes 类型
    NamedDomainObjectContainer<BuildTypes> buildTypes;

    public MyExtension(Project project) {
    	// 创建 NamedDomainObjectContainer
        NamedDomainObjectContainer<BuildTypes> buildTypeObjs = project.container(BuildTypes.class);
        buildTypes = buildTypeObjs;
    }


    // 选用闭包形式形式，也可用
    void setBuildTypes(Closure config){
        ConfigureUtil.configure(config, buildTypes);
    }


    public NamedDomainObjectContainer<BuildTypes> getBuildTypes() {
        return buildTypes;
    }

    // 方式一： 参数为Action
    void setDefaultConfig(Action<DefaultConfig> action) {
        action.execute(defaultConfig);
    }

    // 方式二： 闭包形式
    void setDefaultConfig(Closure config) {
        ConfigureUtil.configure(config, defaultConfig);
    }

    // 方式三： 直接使用属性名作为方法名来对属性进行赋值
    void defaultConfig(Action<DefaultConfig> action) {
        action.execute(defaultConfig);
    }
}