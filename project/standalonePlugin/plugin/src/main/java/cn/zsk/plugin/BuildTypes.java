package cn.zsk.plugin;

public class BuildTypes {
    // 必须有 String name 属性，且不允许构造后修改
    private String name;

    // 业务参数
    private boolean signingConfig;

    // 必须带有以 String name 为参数的 public 构造函数
    public BuildTypes(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isSigningConfig() {
        return signingConfig;
    }

    public void setSigningConfig(boolean signingConfig) {
        this.signingConfig = signingConfig;
    }
}
