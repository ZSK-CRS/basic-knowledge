package com.zsk.plugin;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

public class MyTask2 extends DefaultTask {
    @TaskAction
    void taskAction() {
        //taskAction名字可自定义
        System.out.println("插件任务 MyTask2 执行！");
    }
}
