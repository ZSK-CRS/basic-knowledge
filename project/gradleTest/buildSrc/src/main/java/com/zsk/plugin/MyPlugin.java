package com.zsk.plugin;  
  
import org.gradle.api.Plugin;  
import org.gradle.api.Project;  
import org.gradle.api.Action;  
  
public class MyPlugin implements Plugin<Project> {  
    @Override  
    public void apply(Project project) {  
        System.out.println("这是我的第一个插件!");  
  
        project.getTasks().create("MyTask", task -> {
            task.doLast(task1 -> System.out.println("插件任务 MyTask 执行！"));
        });

        project.getTasks().create("MyTask2", MyTask2.class);

    }  
}